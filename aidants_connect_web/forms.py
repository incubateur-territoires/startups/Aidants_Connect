import re

from django import forms
from django.conf import settings
from django.contrib.auth import password_validation
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.core.exceptions import NON_FIELD_ERRORS, ValidationError
from django.core.validators import EmailValidator, RegexValidator
from django.forms import EmailField
from django.utils.translation import gettext_lazy as _

from django_otp import match_token
from magicauth.forms import EmailForm as MagicAuthEmailForm

from aidants_connect_common.forms import AcPhoneNumberField, PatchedForm
from aidants_connect_common.utils.constants import AuthorizationDurations as ADKW
from aidants_connect_common.widgets import DetailedRadioSelect
from aidants_connect_web.constants import RemoteConsentMethodChoices
from aidants_connect_web.models import (
    Aidant,
    CarteTOTP,
    HabilitationRequest,
    Organisation,
    Usager,
    UsagerQuerySet,
)
from aidants_connect_web.widgets import MandatDemarcheSelect, MandatDureeRadioSelect


class AidantCreationForm(forms.ModelForm):
    """
    A form that creates an aidant, with no privileges, from the given email and
    password.
    """

    password = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.PasswordInput(attrs={"autocomplete": "new-password"}),
        help_text=password_validation.password_validators_help_text_html(),
    )
    first_name = forms.CharField(label="Prénom")
    email = forms.EmailField(label="Email", widget=forms.EmailInput())
    username = forms.CharField(required=False)
    last_name = forms.CharField(label="Nom de famille")
    profession = forms.CharField(label="Profession")

    class Meta:
        model = Aidant
        fields = (
            "email",
            "last_name",
            "first_name",
            "profession",
            "phone",
            "organisation",
            "username",
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["organisation"].required = True

    def clean(self):
        super().clean()
        cleaned_data = self.cleaned_data
        aidant_email = cleaned_data.get("email")
        if aidant_email in Aidant.objects.all().values_list("username", flat=True):
            self.add_error(
                "email", forms.ValidationError("This email is already taken")
            )
        else:
            cleaned_data["username"] = aidant_email

        return cleaned_data

    def _post_clean(self):
        super()._post_clean()
        # Validate the password after self.instance is updated with form data
        # by super().
        password = self.cleaned_data.get("password")
        if password:
            try:
                password_validation.validate_password(password, self.instance)
            except forms.ValidationError as error:
                self.add_error("password", error)

    def save(self, commit=True):
        aidant = super().save(commit=False)
        aidant.set_password(self.cleaned_data["password"])
        if commit:
            aidant.save()
        return aidant


class AidantChangeForm(forms.ModelForm):
    password = ReadOnlyPasswordHashField(
        label=_("Password"),
        help_text=_(
            "Raw passwords are not stored, so there is no way to see this "
            "aidant’s password, but you can change the password using "
            '<a href="../password/">this form</a>.'
        ),
    )

    class Meta:
        model = Aidant
        fields = (
            "email",
            "last_name",
            "first_name",
            "profession",
            "phone",
            "organisation",
            "username",
        )
        field_classes = {"email": EmailField}

    def clean_password(self):
        # Regardless of what the aidant provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial.get("password")

    def clean(self):
        super().clean()
        cleaned_data = self.cleaned_data
        data_email = cleaned_data.get("email")
        initial_email = self.instance.email
        initial_id = self.instance.id

        if data_email != initial_email:
            if (
                Aidant.objects.filter(email__iexact=data_email).exists()
                or Aidant.objects.exclude(id=initial_id)
                .filter(username__iexact=data_email)
                .exists()
            ):
                self.add_error(
                    "email", forms.ValidationError("This email is already taken")
                )
            else:
                cleaned_data["username"] = data_email

        return cleaned_data


class LoginEmailForm(MagicAuthEmailForm):
    email = forms.EmailField()

    def clean_email(self):
        user_email = super().clean_email()
        if not Aidant.objects.filter(email__iexact=user_email, is_active=True).exists():
            raise ValidationError(
                "Votre compte existe mais il n’est pas encore actif. "
                "Si vous pensez que c’est une erreur, prenez contact avec votre "
                "responsable ou avec Aidants Connect."
            )
        return user_email


def get_choices_for_remote_method():
    from django.conf import settings

    remote_choices = RemoteConsentMethodChoices.choices
    if settings.FF_ACTIVATE_SMS_CONSENT:
        return remote_choices
    else:
        return [
            (key, value)
            for key, value in remote_choices
            if key != RemoteConsentMethodChoices.SMS.name
        ]


class MandatForm(PatchedForm):
    demarche = forms.MultipleChoiceField(
        choices=[(key, value) for key, value in settings.DEMARCHES.items()],
        required=True,
        widget=MandatDemarcheSelect,
        error_messages={
            "required": _("Vous devez sélectionner au moins une démarche.")
        },
    )

    DUREES = [
        (
            ADKW.SHORT,
            {"label": "Mandat court", "description": "(expire demain)"},
        ),
        (
            ADKW.MONTH,
            {
                "label": "Mandat d'un mois",
                "description": f"({ADKW.DAYS[ADKW.MONTH]} jours)",
            },
        ),
        (
            ADKW.LONG,
            {"label": "Mandat long", "description": "(12 mois)"},
        ),
        (
            ADKW.SEMESTER,
            {
                "label": "Mandat de 6 mois",
                "description": f"({ADKW.DAYS[ADKW.SEMESTER]} jours)",
            },
        ),
    ]
    duree = forms.ChoiceField(
        label="Choisissez la durée du mandat",
        choices=DUREES,
        required=True,
        initial=3,
        error_messages={"required": _("Veuillez sélectionner la durée du mandat.")},
        widget=MandatDureeRadioSelect,
    )

    is_remote = forms.BooleanField(
        label="Signature à distance du mandat",
        label_suffix="",
        required=False,
    )

    remote_constent_method = forms.ChoiceField(
        label="Sélectionnez une méthode de consentement à distance",
        choices=get_choices_for_remote_method,
        required=False,
        error_messages={
            "required": _(
                "Veuillez sélectionner la méthode de consentement à distance."
            )
        },
        widget=DetailedRadioSelect,
    )

    user_phone = AcPhoneNumberField(
        label="Numéro de téléphone de la personne accompagnée",
        label_suffix=" :",
        initial="",
        required=False,
    )

    user_remote_contact_verified = forms.BooleanField(
        required=False,
        label=(
            "Je certifie avoir validé l’identité de l’usager répondant au numéro de "
            "téléphone qui recevra la demande de consentement par SMS."
        ),
        label_suffix="",
    )

    def clean_remote_constent_method(self):
        if not self.cleaned_data["is_remote"]:
            return ""

        if not self.cleaned_data.get("remote_constent_method"):
            self.add_error(
                "remote_constent_method",
                _(
                    "Vous devez choisir parmis l'une des "
                    "méthodes de consentement à distance."
                ),
            )
            return ""

        return self.cleaned_data["remote_constent_method"]

    def clean_user_phone(self):
        if (
            not self.cleaned_data["is_remote"]
            or self.cleaned_data.get("remote_constent_method")
            != RemoteConsentMethodChoices.SMS.name
        ):
            return ""

        if not self.cleaned_data.get("user_phone"):
            self.add_error(
                "user_phone",
                _(
                    "Un numéro de téléphone est obligatoire "
                    "si le consentement est demandé par SMS."
                ),
            )
            return ""

        return self.cleaned_data["user_phone"]

    def clean_user_remote_contact_verified(self):
        if (
            not self.cleaned_data["is_remote"]
            or self.cleaned_data.get("remote_constent_method")
            not in RemoteConsentMethodChoices.blocked_methods()
        ):
            return True

        if not self.cleaned_data.get("user_remote_contact_verified"):
            raise ValidationError(
                self.fields["user_remote_contact_verified"].error_messages["required"],
                code="required",
            )

        return True


class OTPForm(forms.Form):
    otp_token = forms.CharField(
        max_length=6,
        min_length=6,
        validators=[RegexValidator(r"^\d{6}$")],
        label=(
            "Entrez le code à 6 chiffres généré par votre téléphone "
            "ou votre carte Aidants Connect"
        ),
        widget=forms.TextInput(attrs={"autocomplete": "off"}),
    )

    def __init__(self, aidant, *args, **kwargs):
        super(OTPForm, self).__init__(*args, **kwargs)
        self.aidant = aidant

    def clean_otp_token(self):
        otp_token = self.cleaned_data["otp_token"]
        aidant = self.aidant
        good_token = match_token(aidant, otp_token)
        if good_token:
            return otp_token
        else:
            raise ValidationError("Ce code n'est pas valide.")


class RecapMandatForm(OTPForm, forms.Form):
    personal_data = forms.BooleanField()


class CarteOTPSerialNumberForm(forms.Form):
    serial_number = forms.CharField()

    def clean_serial_number(self):
        serial_number = self.cleaned_data["serial_number"]
        try:
            carte = CarteTOTP.objects.get(serial_number=serial_number)
        except CarteTOTP.DoesNotExist:
            raise ValidationError(
                "Aucune carte n'a été trouvée avec ce numéro de série."
            )
        if carte.aidant:
            raise ValidationError("Cette carte est déjà associée à un aidant.")
        return serial_number


class CarteTOTPValidationForm(forms.Form):
    otp_token = forms.CharField(
        max_length=6,
        min_length=6,
        validators=[RegexValidator(r"^\d{6}$")],
        widget=forms.TextInput(attrs={"autocomplete": "off"}),
    )


class RemoveCardFromAidantForm(forms.Form):
    reason = forms.ChoiceField(
        choices=(
            ("perte", "Perte : La carte a été perdue."),
            ("casse", "Casse : La carte a été détériorée."),
            (
                "dysfonctionnement",
                "Dysfonctionnement : La carte ne fonctionne pas ou plus.",
            ),
            ("depart", "Départ : L’aidant concerné quitte la structure."),
            ("erreur", "Erreur : J’ai lié cette carte à ce compte par erreur."),
            ("autre", "Autre : Je complète ci-dessous."),
        )
    )
    other_reason = forms.CharField(required=False)


class SwitchMainAidantOrganisationForm(forms.Form):
    organisation = forms.ModelChoiceField(
        queryset=Organisation.objects.none(),
        widget=forms.RadioSelect,
    )
    next_url = forms.CharField(required=False)

    def __init__(self, aidant: Aidant, next_url="", *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.aidant = aidant
        self.fields["organisation"].queryset = Organisation.objects.filter(
            aidants=self.aidant
        ).order_by("name")
        self.initial["organisation"] = self.aidant.organisation
        self.initial["next_url"] = next_url


class AddOrganisationResponsableForm(forms.Form):
    candidate = forms.ModelChoiceField(queryset=Aidant.objects.none())

    def __init__(self, organisation: Organisation, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["candidate"].queryset = organisation.aidants.exclude(
            responsable_de=organisation
        ).order_by("last_name")


class ChangeAidantOrganisationsForm(forms.Form):
    organisations = forms.ModelMultipleChoiceField(
        queryset=Organisation.objects.none(),
        widget=forms.CheckboxSelectMultiple,
        error_messages={
            "required": "Vous devez rattacher l’aidant à au moins une organisation."
        },
    )

    def __init__(self, responsable: Aidant, aidant: Aidant, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.aidant = aidant
        self.responsable = responsable
        self.fields["organisations"].queryset = Organisation.objects.filter(
            responsables=self.responsable
        ).order_by("name")
        self.initial["organisations"] = self.aidant.organisations.all()


class HabilitationRequestCreationForm(forms.ModelForm):
    def __init__(self, responsable, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.responsable = responsable
        self.fields["organisation"] = forms.ModelChoiceField(
            queryset=Organisation.objects.filter(
                responsables=self.responsable
            ).order_by("name"),
            empty_label="Choisir...",
        )

    class Meta:
        model = HabilitationRequest
        fields = (
            "email",
            "last_name",
            "first_name",
            "profession",
            "organisation",
        )
        error_messages = {
            NON_FIELD_ERRORS: {
                "unique_together": (
                    "Une demande d’habilitation est déjà en cours pour cette adresse "
                    "e-mail. Vous n’avez pas besoin d’en déposer une nouvelle."
                ),
            }
        }

    def clean_email(self):
        return self.cleaned_data.get("email").lower()


class DatapassForm(forms.Form):
    data_pass_id = forms.IntegerField()
    organization_name = forms.CharField()
    organization_siret = forms.IntegerField()
    organization_address = forms.CharField()
    organization_postal_code = forms.CharField()
    organization_type = forms.CharField()


class ValidateCGUForm(forms.Form):
    agree = forms.BooleanField(
        label="J’ai lu et j’accepte les conditions d’utilisation Aidants Connect.",
        required=True,
    )


class DatapassHabilitationForm(forms.ModelForm):
    data_pass_id = forms.IntegerField()

    class Meta:
        model = HabilitationRequest
        fields = [
            "first_name",
            "last_name",
            "email",
            "profession",
        ]

    def clean_data_pass_id(self):
        data_pass_id = self.cleaned_data["data_pass_id"]
        organisations = Organisation.objects.filter(data_pass_id=data_pass_id)
        if not organisations.exists():
            raise ValidationError("No organisation for data_pass_id")
        self.cleaned_data["organisation"] = organisations[0]

    def clean_email(self):
        email = self.cleaned_data.get("email")
        if email:
            return email.lower()
        return email

    def save(self, commit=True):
        self.instance.organisation = self.cleaned_data["organisation"]
        self.instance.origin = HabilitationRequest.ORIGIN_DATAPASS
        return super().save(commit)


class MassEmailHabilitatonForm(forms.Form):
    email_list = forms.Field(widget=forms.Textarea)

    def clean_email_list(self):
        email_list = self.cleaned_data.get("email_list")
        validate_email = EmailValidator(
            message="Veuillez saisir uniquement des adresses e-mail valides."
        )

        def is_email_valid(value):
            validate_email(value)
            return True

        return set(
            filter(
                is_email_valid,
                (filter(None, (email.strip() for email in email_list.splitlines()))),
            )
        )


class AuthorizeSelectUsagerForm(PatchedForm):
    chosen_usager = forms.IntegerField(
        required=True,
        error_messages={
            "required": (
                required_msg := (
                    "Aucun profil n'a été trouvé."
                    "Veuillez taper le nom d'une personne et la barre de recherche et "
                    "sélectionner parmis les propositions dans la liste déroulante"
                )
            ),
            "invalid": required_msg,
        },
    )

    def __init__(self, usager_with_active_auth: UsagerQuerySet, *args, **kwargs):
        self.usager_with_active_auth = usager_with_active_auth
        super().__init__(*args, **kwargs)

    def clean_chosen_usager(self):
        chosen_usager = self.cleaned_data.get("chosen_usager")
        try:
            user = Usager.objects.get(pk=chosen_usager)
            if user not in self.usager_with_active_auth:
                raise ValidationError("", code="unauthorized_user")
            return user
        except (Usager.DoesNotExist, Usager.MultipleObjectsReturned):
            raise ValidationError(
                "La personne sélectionnée ne semble pas exister", code="invalid"
            )


class OAuthParametersForm(PatchedForm):
    state = forms.CharField()
    nonce = forms.CharField()
    response_type = forms.CharField()
    client_id = forms.CharField()
    redirect_uri = forms.CharField()
    scope = forms.CharField()
    acr_values = forms.CharField()

    def clean_nonce(self):
        result = self.cleaned_data.get("nonce")
        if result and not result.isalnum():
            raise ValidationError("", "invalid")
        return result

    def clean_state(self):
        result = self.cleaned_data.get("state")
        if result and not result.isalnum():
            raise ValidationError("", "invalid")
        return result

    def clean_response_type(self):
        result = self.cleaned_data.get("response_type")
        if result != "code":
            raise ValidationError("", "invalid")
        return result

    def clean_client_id(self):
        result = self.cleaned_data.get("client_id")
        if result != settings.FC_AS_FI_ID:
            raise ValidationError("", "invalid")
        return result

    def clean_redirect_uri(self):
        result = self.cleaned_data.get("redirect_uri")
        if result != settings.FC_AS_FI_CALLBACK_URL:
            raise ValidationError("", "invalid")
        return result

    def clean_scope(self):
        result = self.cleaned_data.get("scope")
        splitted = re.split(r"\s+", result)
        splitted.sort()
        if splitted != ["address", "birth", "email", "openid", "phone", "profile"]:
            raise ValidationError("", "invalid")
        return result

    def clean_acr_values(self):
        result = self.cleaned_data.get("acr_values")
        if result != "eidas1":
            raise ValidationError("", "invalid")
        return result

    def __init__(self, *args, relaxed=False, **kwargs):
        self.relaxed = relaxed
        super().__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super().clean()

        if self.relaxed:
            return cleaned_data

        additionnal_keys = set(self.data.keys()) - set(self.fields.keys())

        for additionnal_key in additionnal_keys:
            self.add_error(None, ValidationError(additionnal_key, "additionnal_key"))

        return cleaned_data


class SelectDemarcheForm(PatchedForm):
    chosen_demarche = forms.CharField()

    def __init__(self, aidant: Aidant, user: Usager, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.aidant = aidant
        self.user = user

    def clean_chosen_demarche(self):
        result = self.cleaned_data["chosen_demarche"]
        if not self.aidant.get_valid_autorisation(result, self.user):
            raise ValidationError("", code="unauthorized_demarche")
        return result
